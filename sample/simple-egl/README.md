Information
====
<br>This is a sample application for homescreen-2017 and windowmanager-2017.

How to compile and install
====
<br>$ mkdir build
<br>$ cd build
<br>$ cmake ..
<br>$ make
<br>$ make widget

<br>Copy package/simple-egl.wgt to rootfs.

How to use
====
<br>afm-util install simple-egl.wgt
<br>afm-util start simple-egl@0.1

Depends
====
<br>homescreen-2017
<br>agl-service-homescreen-2017
<br>agl-service-windowmanger-2017
<br>libhomescreen
<br>libwindowmanager
<br>wayland-ivi-extension
