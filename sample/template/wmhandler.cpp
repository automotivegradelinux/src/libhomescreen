/*
 * Copyright (c) 2017 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "wmhandler.h"
#include <unistd.h>


void WmHandler::init(LibWindowmanager *p_wm, std::string applabel)
{
    mp_wm = p_wm;
    m_applabel = applabel;
}

void WmHandler::slotActivateSurface() {
    json_object *obj = json_object_new_object();
    json_object_object_add(obj, wm->kKeyDrawingName, json_object_new_string(m_applabel.c_str()));
    json_object_object_add(obj, wm->kKeyDrawingArea, json_object_new_string("normal.full"));
    mp_wm->activateSurface(obj);
}

WmHandler::WmHandler(QObject *parent)
    :QObject(parent)
{
}

WmHandler::~WmHandler() { }
