This project contains:

libhomescreen: HomeScreen API C++ shared library

AGL repo for source code:
https://gerrit.automotivelinux.org/gerrit/p/src/libhomescreen.git

AGL repo for bitbake recipe:
https://gerrit.automotivelinux.org/gerrit/gitweb?p=AGL/meta-agl-devel.git;a=blob;f=recipes-demo-hmi/libhomescreen/libhomescreen_git.bb

Quickstart:

Instructions for building libhomescreen
---------------------------------------

The libhomescreen is part of the 
packagegroup-hmi-framework
packagegroup.

This also includes the following apps:
- agl-service-windowmanager-2017
- agl-service-soundmanager-2017
- agl-service-homescreen-2017
- homescreen-2017
- onscreenapp-2017

To build all the above, follow the instrucions on the AGL
source meta-agl/scripts/aglsetup.sh -m m3ulcb agl-demo agl-devel agl-appfw-smack agl-hmi-framework
bitbake agl-demo-platform
